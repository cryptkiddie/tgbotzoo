from time import time, sleep
import re
from random import randint
from configparser import ConfigParser
from pyrogram import Client, filters

config = ConfigParser()
config.read("config.ini")
block = {}

bot = Client(
    "bot",
    bot_token=config["pyrogram"]["bot_token"]
    if "bot_token" in config["pyrogram"]
    else None,
    api_id=config["pyrogram"]["api_id"]
    if "api_id" in config["pyrogram"]
    else None,
    api_hash=config["pyrogram"]["api_hash"]
    if "api_hash" in config["pyrogram"]
    else None,
)


@bot.on_message(filters.command("start"))
def start(client, message):
    message.reply_text(
        f'Hallo ich bin {config["general"]["name"]}, füg mich zu deiner Gruppe hinzu und habe spass.'
    )


@bot.on_message(filters.command("license"))
def license(client, message):
    message.reply_text(
        "Ich bin stehe unter der MIT license. Hier ist mein repository https://gitlab.com/ulpa/tgbotzoo"
    )


@bot.on_message(filters.text)
def handle(client, message):
    if message.chat.id not in block or block[message.chat.id] < time():
        for key in config["trigger"]:
            probabilty = re.findall(r"p{\d+}", str(config["trigger"][key]))
            if probabilty:
                if (
                    randint(1, int(probabilty[0].replace("p{", "").replace("}", "")))
                    == 1
                ):
                    message.reply_text(
                        config["trigger"][key].replace(probabilty[0], "")
                    )
            elif re.findall(str(key), message.text):
                slp = re.findall(r"s{\d+}", str(config["trigger"][key]))
                delay = re.findall(r"d{\d+}", str(config["trigger"][key]))
                text = config["trigger"][key]
                cnt = "c{}" in text
                if cnt:
                    text = text.replace("c{}", "")
                if delay:
                    sleep(int(delay[0].replace("d{", "").replace("}", "")))
                    text = text.replace(delay[0], "")
                if slp:
                    text = text.replace(slp[0], "")

                    block[message.chat.id] = time() + int(
                        slp[0].replace("s{", "").replace("}", "")
                    )

                message.reply_text(text, quote=False, disable_notification=True)
                if not cnt:
                    return


bot.run()
